from fastapi import FastAPI

from authentification import controllers

app = FastAPI()
app.include_router(controllers.router)
