from pydantic import BaseModel
from pydantic import validator


def not_empty(field_name, val):
    if not val:
        raise ValueError(f'{field_name} error, token is empty')
    return val


class AuthModel(BaseModel):
    token: str
    refresh_token: str

    @validator('token')
    def token_not_empty(cls, v):
        return not_empty('token', v)

    @validator('refresh_token')
    def ref_token_not_empty(cls, v):
        return not_empty('refresh_token', v)


class LoginModel(BaseModel):
    login: str
    password: str
