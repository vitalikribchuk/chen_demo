from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from pydantic import ValidationError

from authentification.models import AuthModel
from authentification.models import LoginModel

router = APIRouter()


def get_data_from_third_party(login_obj: LoginModel) -> dict:
    if login_obj.login and login_obj.password:
        return {
            'token': 'some_token',
            'refresh_token': 'some_refresh_token'
        }
    return {}


@router.get("/")
async def home():
    return 'Home'


@router.get("/login", response_model=AuthModel)
async def login(login_obj: LoginModel = Depends()):
    data = get_data_from_third_party(login_obj)
    try:
        return AuthModel(**data['auth_response'])
    except KeyError as e:
        pass  # do somesing when auth_response does not exist
    except ValidationError as e:
        raise HTTPException(status_code=422, detail=e.errors())


class MyClass:
    a = ""


my_dict = {"a": "some_value"}


